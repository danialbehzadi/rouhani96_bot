#!/bin/bash
source .env/bin/activate
until ./r96bot.py; do
    echo "'r96bot.py' crashed with exit code $?. Restarting..." >&2
    sleep 1
done
