Requirements
============
you need `libjpeg-dev` and `zlib1g-dev` packages to be installed on your computer, before installing. You should make an ascii text file named `token` in same directory which includes just the token you got from Telegram Botfather.

Install
=======
To make it work, do the following:

    $ git clone https://danialbehzadi@gitlab.com/danialbehzadi/rouhani96_bot.git
    $ cd rouhani96_bot
    $ virtualenv -p python3 --no-site-packages --distribute .env
    $ source .env/bin/activate
    $ pip3 install -r requirements.txt

Run
===
To run the bot just run:

    $ nohup ./run.sh &
